﻿using SiteTestBLL.Servises.XmlServises;
using SiteTestDAL;
using SiteTestDAL.Model;
using SiteTestDAL.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Xml;


namespace SiteTestBLL.Servises
{
    public class PerfomanseRequest
    {
        
        private XmlRecipient Xml { get; }
        public static List<PagesResponse> ticks = new List<PagesResponse>();
        private static List<string> urls = new List<string>();
        private static string url { get; set; }
        private static int sitesLeft { get; set; }
        private DbRequestServise repository;

        public PerfomanseRequest(string _url)
        {
            Xml = new XmlRecipient(_url);
            url = _url;
            repository = new DbRequestServise();
        }            
        static void ThreadMethod(object name)
        {
            List<double> listOfTicks = new List<double>();
            bool avaliable = true;

            Stopwatch timer = new Stopwatch();
            for (int i = 0; i < 6; ++i)
            {
                try
                {
                    WebRequest request = WebRequest.Create((string)name);
                    timer.Start();
                    WebResponse response = request.GetResponse();
                    timer.Stop();
                    listOfTicks.Add(timer.ElapsedMilliseconds);
                    timer.Reset();
                    response.Close();
                }
                catch (WebException)
                {
                    listOfTicks.Clear();
                    listOfTicks.Add(000);
                    listOfTicks.Add(000);
                    avaliable = false;
                    break;
                }
            }
            listOfTicks.Sort();           

            double summOfTicks = 0;
            for(int i = 0; i < listOfTicks.Count; ++i)
            {
                summOfTicks += listOfTicks[i];
            }
            double middle;
            if (summOfTicks != 0)
                middle = summOfTicks / listOfTicks.Count;
            else middle = 000;

            ticks.Add(new PagesResponse {Id=Guid.NewGuid(),
                PageName = (string)name,
                ResponseTimeMin =listOfTicks.First(),
                ResponseTimeMax =listOfTicks.Last(),
                ResponseTimeMiddle = middle,
                Access = avaliable
            });
            urls.Remove((string)name);         
        }

        public List<PagesResponse> GetPerfomanse()
        {
            urls = Xml.TakeSiteMap();
            if (urls.First() == "1")
            {
                PagesResponse message = new PagesResponse { PageName = urls[0] };
                ticks.Add(message);
                message = new PagesResponse { PageName = urls[1] };
                ticks.Add(message);
                return ticks;
            }
            else
            {

                foreach (string name in urls)
                {
                    ThreadPool.SetMaxThreads(60, 60);
                    ThreadPool.SetMinThreads(60, 60);
                    ThreadPool.QueueUserWorkItem(ThreadMethod, name);
                }
                while (urls.Count != 0)
                {

                }
                repository.SaveTesting(ticks, url);
                return ticks.OrderByDescending(x => x.ResponseTimeMiddle).ToList();
            }
        }        
    }
}
