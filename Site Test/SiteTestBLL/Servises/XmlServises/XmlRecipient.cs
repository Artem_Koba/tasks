﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace SiteTestBLL.Servises.XmlServises
{
    public class XmlRecipient
    {

        private string siteUri { get; set; }  
        private XmlDocument initialSiteMap = new XmlDocument();
        List<string> urls = new List<string>();


        public XmlRecipient(string _siteUri)
        {
            siteUri = _siteUri;                                         
        }
        
        public List<string> TakeSiteMap()
        {
            try
            {
                initialSiteMap.Load(siteUri + "sitemap.xml");
            }
            catch (Exception)
            {
                urls.Add("1");
                urls.Add($"Error, can not load sitemap.xml from the adress {siteUri}");
                return urls;
            }
            
            XmlElement element = initialSiteMap.DocumentElement;
            
            foreach (XmlNode node in element)
            {                
                foreach (XmlNode child in node.ChildNodes)
                {                    
                    if (child.Name == "loc")
                    {
                        urls.Add(child.InnerText);
                    }                    
                }
            }
            return urls;

        }        
    }
}
