﻿using SiteTestDAL.Model;
using SiteTestDAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteTestBLL.Servises
{
    public class DbRequestServise
    {
        private GenericRepository<SiteInfo> repository;

        public DbRequestServise()
        {
            repository = new GenericRepository<SiteInfo>();
        }
        public void SaveTesting(List<PagesResponse>pages,string url)
        {
            repository.Post(new SiteInfo { Id = Guid.NewGuid(),  PagesResponses = pages, SiteName = url });
        }

        public List<SiteInfo> GetTestedSitesNameAndId()
        {
            return repository.GetAllNamesAndId();
        }

        public SiteInfo GetSiteHistory(Guid Id)
        {
            return repository.Get(Id);
        }
    }
}
