﻿using SiteTestDAL.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteTestDAL
{
    public class SiteContext : DbContext
    {
        public SiteContext() : base("SiteTestDb")
        { }
        public DbSet<PagesResponse> pagesResponses { get; set; }
        public DbSet<SiteInfo> siteInfos { get; set; }
    }
}
