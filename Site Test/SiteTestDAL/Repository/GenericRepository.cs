﻿using SiteTestDAL.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteTestDAL.Repository
{
    public class GenericRepository<TEntity>
        where TEntity : class
    {
        SiteContext _context;
        DbSet<TEntity> _dbset;
        public GenericRepository ()
        {
            _context = new SiteContext();
            _dbset = _context.Set<TEntity>();
        }
        
        public void Post(TEntity model)
        {            
            _dbset.Add(model);
            _context.SaveChanges();            
        }
        
        public List<SiteInfo> GetAllNamesAndId()
        {

            var s = _context.siteInfos
                .Select(x => new
                {
                Id = x.Id,
                SiteName = x.SiteName                
        }).ToList();
            List<SiteInfo> v = s.Select(f=> new SiteInfo
            {
                Id = f.Id,
                SiteName = f.SiteName
            }).ToList();
            return v;

        }

        public TEntity Get(Guid Id)
        {
            return _dbset.Find(Id);
        }
    }
}
