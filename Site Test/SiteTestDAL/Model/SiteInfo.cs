﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteTestDAL.Model
{
    public class SiteInfo
    {        
        public Guid Id { get; set; }
        public string SiteName { get; set; }        

        public virtual List<PagesResponse> PagesResponses {get;set;}
    }
}
