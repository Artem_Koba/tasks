﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteTestDAL.Model
{
    public class PagesResponse
    {        
        public Guid Id { get; set; }
        public string PageName { get; set; }
        public double ResponseTimeMin { get; set; }
        public double ResponseTimeMax { get; set; }
        public double ResponseTimeMiddle { get; set; }
        public bool Access { get; set; }

        public SiteInfo SiteInfo { get; set; }
    }
}
