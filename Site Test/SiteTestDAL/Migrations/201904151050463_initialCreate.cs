namespace SiteTestDAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PagesResponses",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PageName = c.String(),
                        ResponseTimeMin = c.Double(nullable: false),
                        ResponseTimeMax = c.Double(nullable: false),
                        ResponseTimeMiddle = c.Double(nullable: false),
                        Access = c.Boolean(nullable: false),
                        SiteInfo_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SiteInfoes", t => t.SiteInfo_Id)
                .Index(t => t.SiteInfo_Id);
            
            CreateTable(
                "dbo.SiteInfoes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        SiteName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PagesResponses", "SiteInfo_Id", "dbo.SiteInfoes");
            DropIndex("dbo.PagesResponses", new[] { "SiteInfo_Id" });
            DropTable("dbo.SiteInfoes");
            DropTable("dbo.PagesResponses");
        }
    }
}
