﻿using SiteTestBLL.Servises;
using SiteTestDAL.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace Site_Test.Controllers
{
    public class HomeController : Controller
    {        
        PerfomanseRequest request;
        DbRequestServise db = new DbRequestServise();
        List<PagesResponse> model;


        [HttpGet]
        public ActionResult Index()
        {
            var history = db.GetTestedSitesNameAndId();
            if (history.Count == 0)
            {
                ViewBag.History = null;
            }
            else
                ViewBag.History = history;
            return View("Index");
        }

        [HttpPost]
        public ActionResult Start(string url)
        {            
            request = new PerfomanseRequest(url);
            model = request.GetPerfomanse();
            if (model.First().PageName == "1")
            {
                ViewBag.Message = model[1];                
                return View("Error");
            }
            else
            {                
                ViewBag.Pages = model;
                ViewBag.Name = url;
                return View("Analising");
            }
        }        
    }
}