﻿using SiteTestBLL.Servises;
using SiteTestDAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace Site_Test.Controllers
{
    public class HistoryController : Controller
    {
        DbRequestServise dbRequest = new DbRequestServise();
        // GET: api/History/5
        public ActionResult Get(Guid Id)
        {
            ViewBag.Pages = dbRequest.GetSiteHistory(Id);            
            return View("Analising"); 
        }
    }
}
